// const GenerateJSON = require('./GenerateCGJSON');
const _ = require('lodash');
const request = require('request');
const Excel = require('exceljs');
const pixelWidth = require('string-pixel-width');
const AWS = require('aws-sdk');
const fs = require('fs');
const path = require('path');
const moment = require('moment');
const mtz = require('moment-timezone');
const slugify = require('@sindresorhus/slugify');
const RESTAURANT_COMMISSION_GRID_ENTITY_ID = "5bceffb7c6ff750411db480c";
const RESTAURANT_ORDER_ENTITY_ID = "5825d66df009c4bc37fd7bae";
const GOKHANA_APPID = "5825d15cf009c4bc37fd7b9f";
const { Client } = require('@elastic/elasticsearch');
const analyticsclient = new Client({ node: 'https://analytics.ampcome.com' });
var configJSON = require(path.normalize(path.join(__dirname, 'config.json')))
var xlsFolderPath = "/tmp/";
const SparkPost = require('sparkpost');
const sparkclient = new SparkPost('8ef434e19985c8db573fd9cdeddbc2e7e066177f');
mtz.tz.setDefault("Asia/Kolkata");
// var fromdate = '';
// var todate = '';
// var emails = [];

var html = "<div class=\"invoice-box\"><br /><table cellspacing=\"0\" cellpadding=\"0\"><tbody><tr class=\"top\"><td colspan=\"2\"><table><tbody><tr><td class=\"title\"><img style=\"width: 100%; max-width: 300px;\" src=\"https://res.cloudinary.com/ampcome/image/upload/v1489040892/gokhana_logo_rgf7us.png\" /></td><td>&nbsp;</td><td>&nbsp;</td></tr></tbody></table></td></tr><tr class=\"information\"><td colspan=\"3\"><h3 style=\"color: #d37a11;\">Hello,</h3><p>This is the settlement report from {{ fromdate }} to {{ todate }}.</p></td></tr>   {{ if not empty(locations) }}<tr style=\"background-color:lightgrey;font-weight:bold;\" class=\"heading\"><td style=\"padding-left: 5px;\"><strong>Link</strong></td></tr>  {{ each locations }} <tr class=\"item\"><td style=\" border: 0.2px solid lightgrey;     padding-left: 5px;\">{{loop_var}}</td></tr> {{end}}  {{end}}<tr class=\"information\"><td colspan=\"3\"><p>For more details, go through the attachment and download settlement report.</p><br /> <small>This email was sent from a notification-only address that cannot accept incoming email. Please do not reply to this message.</small> <br /> <small>Copyright &copy; 2017-18, Tobox Venture Private Limited, All rights reserved.</small> <br /> <br /> <a href=\"http://www.gokhana.com/terms.html\">Term of use</a><div style=\"float: right;\"><a href=\"https://www.facebook.com/TeamGoKhana/\" target=\"_blank\" rel=\"noopener\"> <img src=\"https://res.cloudinary.com/ampcome/image/upload/c_scale,h_20/v1520581126/social-media/facebook.png\" alt=\"images\" /> </a> <a href=\"https://twitter.com/TeamGoKhana\" target=\"_blank\" rel=\"noopener\"> <img src=\"https://res.cloudinary.com/ampcome/image/upload/c_scale,h_20/v1520581121/social-media/twitter.png\" alt=\"images\" /> </a> <a href=\"https://www.instagram.com/teamgokhana/\" target=\"_blank\" rel=\"noopener\"> <img src=\"https://res.cloudinary.com/ampcome/image/upload/c_scale,h_20/v1520581136/social-media/instagram.png\" alt=\"images\" /> </a> <a href=\"https://in.pinterest.com/teamgokhana/\" target=\"_blank\" rel=\"noopener\"> <img src=\"http://res.cloudinary.com/ampcome/image/upload/c_scale,h_20/v1520581134/social-media/pinterest.png\" alt=\"images\" /> </a> <a href=\"https://www.linkedin.com/company/13583900/\" target=\"_blank\" rel=\"noopener\"> <img src=\"https://res.cloudinary.com/ampcome/image/upload/c_scale,h_20/v1520581122/social-media/linkedin.png\" alt=\"images\" /> </a></div></td></tr></tbody></table></div>";
var subject = "GoKhana summary report";

var app_setting = { data: { gst_on_commission: 18 } };

AWS.config.update({
    accessKeyId: "AKIAZNG6IY6TJWINYYBG",
    secretAccessKey: "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"
});

var s3 = new AWS.S3();

async function upload(filePath, reportdate, filename, cb) {
    //configuring parameters
    var bucketName = "gokhana-s3";
    var d = new Date(reportdate);
    const foldername = d.getFullYear() + "-" + d.getMonth() + "-" + d.getDate();
    // if (process.env.NODE_ENV) {
    //     bucketName = `gokhana-report-${process.env.NODE_ENV}`
    // }
    var params = {
        Bucket: bucketName,
        Body: fs.createReadStream(filePath),
        Key: foldername + "/" + filename,
        ACL: 'public-read'
    };
    if (cb) {
        s3.upload(params, function (err, data) {
            //handle error
            if (err) {
                console.log("Error", err);
                cb(err)
            }
            //success
            if (data) {
                console.log("Uploaded in:", data);
                cb(null, data);
            }
        });
    } else {
        return new Promise((resolve, reject) => {
            s3.upload(params, function (err, data) {
                //handle error
                if (err) {
                    console.log("2 Error", err);
                    reject(err);
                }
                //success
                if (data) {
                    console.log("2 Uploaded in:", data.Location);
                    // sendEmail(data.Location);
                    resolve(data);
                }
            });
        });
    }
}

function sendEmail(locations, fromdate, todate, emails) {
    return new Promise(function (resolve, reject) {
        let newloc = [];
        newloc.push(locations);
        let attachments = [];
        let substitution_data = {
            fromdate: mtz.tz(fromdate, 'Asia/Kolkata').format('DD MMM YYYY LT'),
            todate: mtz.tz(todate, 'Asia/Kolkata').format('DD MMM YYYY LT'),
            locations: newloc,
            customername: 'Gokhana'
        }
        let rcpts = [{
            address: 'prashant@ampcome.com',
            substitution_data: substitution_data
        }];

        if (isNotEmpty(emails)) {
            rcpts.pop();
            emails.forEach((rcpt) => {
                rcpts.push({ address: rcpt, substitution_data: substitution_data });
            });
        }
        sparkclient.transmissions.send({
            "content": {
                "from": 'info@gokhana.com',
                "html": html,
                "subject": subject,
                "attachments": attachments
            },
            recipients: rcpts
        }).then(() => {
            resolve("Email sent successfully");
        }).catch((er) => {
            reject("Faild");
            // return sendRes(500, er.message.toString());
        });
    });
}

async function sheets(reports, file) {
    if (isNotEmpty(reports) && reports.length > 1) {
        const workbookWriter = new Excel.stream.xlsx.WorkbookWriter({
            filename: file
        })
        let index = 0;
        for (let report of reports) {
            if (isNotEmpty(report)) {
                var workSheet = workbookWriter.addWorksheet('Sheet ' + (index + 1))
                let r = report.reduce((l, o) => (Object.keys(o).length > Object.keys(l).length) ? o : l);
                let columns = []
                Object.keys(r).forEach((k, index) => {
                    columns.push({ header: k, key: k, width: pixelWidth(k) / 40 })
                });
                if (isNotEmpty(columns)) workSheet.columns = columns;
                // let as = 0;
                // for (let x = 0; x < report.length; x++) {
                //     if (report[x]['OrderID']) {
                //         for (let y = x + 1; y < report.length; y++) {
                //             if (report[y]['OrderID']) {
                //                 if (report[x]['OrderID'].toString().trim() === report[y]['OrderID'].toString().trim()) {
                //                     report.splice(y, 1);
                //                     // console.log('removed');
                //                 }
                //             }
                //         }
                //     }
                // }
                for (let row of report) {
                    workSheet.addRow(row).commit();
                }
                workSheet.commit();
                index++;
            }
        }
        return workbookWriter.commit().then(r => {
            console.log(file, " xlsx file created.");
            return r;
        }).catch((err) => { console.error('Issue in workfile creation', err); return null; })
    }
}





async function createAndUploadFinalRecords(results, initiatedat, fromdate, todate) {
    console.log('Got commission calculated reports #', results.length);
    let filename = slugify('all-report' + " " + moment(fromdate).format('YYYYMMDD') + " to " + moment(todate).format('YYYYMMDD')) + "_" + new Date().getTime() + ".xlsx";
    let file = xlsFolderPath + filename;
    return sheets(results, file)
        .then(r => {
            if (fs.existsSync(file)) {
                return upload(file, mtz.tz('Asia/Kolkata').toDate(), filename)
            } else { return Promise.reject(file, " File is not present in local storage to upload to AWS") }
        }).then(res => {
            console.log("Status of uploading file ", filename, " to AWS S3 cloud is", res);
            try {
                fs.unlinkSync(file);
            } catch (e) { console.error(moment().toISOString(), 'Issue in removing file after uploading', file, e) }
            return res
        }).catch(err => {
            console.log("Aws Error", err);
            throw err;
        });
}



function calculateWithOutCommission(orders) {
    let report = [];

    if (isNotEmpty(orders)) {
        orders.forEach((order) => {
            let isNotExistsPreviousRow = true;
            order.data.items.forEach(item => {
                let restaurantdiscountamount;
                let netamount = 0;
                let gokhanadiscountamount = 0;
                let companydiscountamount = 0;
                if (isNotEmpty(order.data.paymenttransactions)) {
                    order.data.paymenttransactions.forEach(pt => {
                        pt.percent = order.data.paidamount > 0 ? (pt.paidamount / order.data.paidamount) * 100 : 0;
                    })
                }
                if (isNotEmpty(order.data.paymenttransactions)) {
                    order.data.paymenttransactions.forEach(transaction => {
                        if (!transaction && order.data.paid && isNotEmpty(order.data.discountsplit) && order.data.discountsplit.reduce((a, o) => a + o.percent, 0) === 100) {
                            transaction = { percent: 0 }
                            order.data.paymenttransactions = []
                            order.data.paymenttransactions.push(transaction);
                        }
                        if (transaction && isBlank(transaction.paymentgateway)) transaction.paymentgateway = "Default";
                        if (transaction && isBlank(transaction.paymenttype)) transaction.paymenttype = "Default";
                        if (transaction) {
                            let data = {};
                            // data["Total"] = "";
                            if (isNotExistsPreviousRow) {
                                data["OrderID"] = order.data.orderid;
                                data["Token No"] = order.data.tokenno;
                                data["Placed Date"] = mtz.tz(order.data.placedtime, 'Asia/Kolkata').format('DD/MM/YYYY');
                                data["Placed Time"] = mtz.tz(order.data.placedtime, 'Asia/Kolkata').format('LTS');
                                data["Customer Name"] = (order.data.customername) != null ? order.data.customername : "";
                                data["Customer Email"] = (order.data.customeremail) != null ? order.data.customeremail : "";
                                data["Customer No."] = (order.data.customerno) != null ? order.data.customerno : "";
                                data["Restaurant"] = order.data.restaurantname;
                                data["Foodcourt"] = order.data.foodcourtname;
                                data["Items Count"] = order.data.itemscount;
                                data["Order Value"] = order.data.totalprice;
                                data["Discount From Credit"] = order.data.discountfromcredit;
                                data["Order Paid Amount"] = order.data.paidamount;
                                data["Status"] = order.data.orderstatus;
                                data["Order ype"] = order.data.ordertype;
                                data["Payment Status"] = order.data.paymentstatus;
                                data["Created platform"] = order.data.createdplatform;
                                data["Channel"] = ((order.data.createdplatform === 'gokhanapartner' && 'Counter')
                                    || (order.data.createdplatform === 'gokhanapartnerfm-kiosk' && 'Kiosk')
                                    || (order.data.createdplatform === 'gokhanapartnerfm' && 'C-Counter')
                                    || (order.data.createdplatform === 'gokhanacustomer' && 'Mobile')
                                    || (order.data.createdplatform === 'gokhanacustomerweb' && 'Web') || "")

                            } else {
                                data = fillBlankData(data);
                            }
                            data["Item Name"] = item.menuitemname;
                            data["Item count"] = item.count;
                            data["Item Price"] = item.totalprice;
                            let gokhanadiscountpercent = 0;
                            let restaurantdiscountpercent = 0;
                            let companydiscountpercent = 0;
                            if (isNotEmpty(order.data.discountsplit)) {
                                order.data.discountsplit.forEach(dis => {
                                    if (dis.percent || dis.percent == 0) dis.percent = order.data.paidamount + order.data.totalCompanyDiscount
                                    if (dis.promotedgroup == "gokhana" || dis.promotedby == "gokhana") {
                                        gokhanadiscountpercent = dis.percent;
                                    } else if (dis.promotedgroup == "restaurant" || dis.promotedby == "restaurant") {
                                        restaurantdiscountpercent = dis.percent;
                                    } else if (dis.promotedgroup == "company" || dis.promotedby == "company") {
                                        companydiscountpercent = dis.percent;
                                    }
                                });
                            }
                            restaurantdiscountpercent = calculateDisPercent(restaurantdiscountpercent, order.data.discountsplit, "restaurant", item.totalprice);
                            restaurantdiscountamount = doubleDecimal((item.totalprice * restaurantdiscountpercent) * 0.01);
                            let gstper = ((item.taxAmount / item.totalprice) * 100);
                            totalgst = restaurantdiscountamount > 0 ? doubleDecimal(((item.totalprice - restaurantdiscountamount) * gstper) * 0.01) : item.taxAmount;
                            netamount = (item.totalprice - restaurantdiscountamount) + totalgst;
                            gokhanadiscountpercent = calculateDisPercent(gokhanadiscountpercent, order.data.discountsplit, "gokhana", netamount);
                            gokhanadiscountamount = doubleDecimal((netamount * gokhanadiscountpercent) * 0.01);
                            companydiscountpercent = calculateDisPercent(companydiscountpercent, order.data.discountsplit, "company", netamount - gokhanadiscountamount);
                            if (companydiscountpercent > 0) companydiscountamount = doubleDecimal(((netamount - gokhanadiscountamount) * companydiscountpercent) * 0.01);
                            let empid;
                            let companyname;
                            empid = order.data.employeeid;
                            if (order.data.company_details && order.data.company_details.data) companyname = order.data.company_details.data.name;
                            data["Restaurant Discount Amount"] = restaurantdiscountamount > 0 ? restaurantdiscountamount : "No discount";
                            data["GST Amount"] = totalgst;
                            data["Net Menu Item Amount"] = netamount;
                            data["Gokhana Discount Amount"] = gokhanadiscountamount > 0 ? gokhanadiscountamount : "No discount";
                            data["Employee Id"] = empid || "";
                            data["Company Name"] = companyname || "";
                            data["Company Discount Amount"] = companydiscountamount > 0 ? companydiscountamount : "No discount";
                            data["Payment Gateway"] = transaction.paymentgateway || '';
                            data["Payment Type"] = transaction.paymenttype || 'Default';
                            let paidamount = doubleDecimal(transaction.percent * (netamount - companydiscountamount - gokhanadiscountamount) * 0.01);
                            data["Amount paid by Customer"] = paidamount;
                            isNotExistsPreviousRow = false;
                            report.push(data);
                        }
                    })
                }
            });
        })
    }
    return report;
}



function calculateCommission(cg, orders, fromdate, todate) {
    let ctr = 0;
    let ids = [];
    let report = [];
    let issuelist = [];
    if (isNotEmpty(orders)) {
        for (let order of orders) {
            if (order.data && isNotEmpty(order.data.items) && order.data.paid) {
                for (let item of order.data.items) {
                    if (item) {
                        let isNotExistsPreviousRow = true;
                        let restaurantdiscountamount;
                        let netamount = 0;
                        let gokhanadiscountamount = 0;
                        let companydiscountamount = 0;
                        // percentage for each payment mode
                        if (isNotEmpty(order.data.paymenttransactions)) {
                            order.data.paymenttransactions.forEach(pt => {
                                pt.percent = order.data.paidamount > 0 ? (pt.paidamount / order.data.paidamount) * 100 : 0;
                            })
                        }
                        if (isEmpty(order.data.paymenttransactions) && order.data.paid && isNotEmpty(order.data.discountsplit) && order.data.discountsplit.reduce((a, o) => a + o.percent, 0) === 100) {
                            transaction = { percent: 0, paymentgateway: "Default", paymenttype: "Default" }
                            order.data.paymenttransactions = []
                            order.data.paymenttransactions.push(transaction);
                        }
                        if (isNotEmpty(order.data.paymenttransactions)) {
                            order.data.paymenttransactions.forEach(transaction => {
                                if (transaction) {
                                    let data = {};
                                    // data["Total"] = "";
                                    if (isNotExistsPreviousRow) {
                                        if (report.every(o => o.OrderID != order.data.orderid)) {
                                            data["OrderID"] = order.data.orderid;
                                            data["Token No"] = order.data.tokenno;
                                            data["Placed Date"] = mtz.tz(order.data.placedtime, 'Asia/Kolkata').format('DD/MM/YYYY');
                                            data["Placed Time"] = mtz.tz(order.data.placedtime, 'Asia/Kolkata').format('LTS');
                                            data["Customer Name"] = (order.data.customername) != null ? order.data.customername : "";
                                            data["Customer Email"] = (order.data.customeremail) != null ? order.data.customeremail : "";
                                            data["Customer No."] = (order.data.customerno) != null ? order.data.customerno : "";
                                            data["Restaurant"] = order.data.restaurantname;
                                            data["Foodcourt"] = order.data.foodcourtname;
                                            data["Items Count"] = order.data.itemscount;
                                            data["Order Value"] = order.data.totalprice;
                                            data["Discount From Credit"] = order.data.discountfromcredit;
                                            data["Order Paid Amount"] = order.data.paidamount;
                                            data["Status"] = order.data.orderstatus;
                                            data["Order Type"] = order.data.ordertype;
                                            data["Payment Status"] = order.data.paymentstatus;
                                            data["Created platform"] = order.data.createdplatform;
                                            data["Channel"] = ((order.data.createdplatform === 'gokhanapartner' && 'Counter')
                                                || (order.data.createdplatform === 'gokhanapartnerfm-kiosk' && 'Kiosk')
                                                || (order.data.createdplatform === 'gokhanapartnerfm' && 'C-Counter')
                                                || (order.data.createdplatform === 'gokhanacustomer' && 'Mobile')
                                                || (order.data.createdplatform === 'gokhanacustomerweb' && 'Web') || "")
                                        } else { data = fillBlankData(data) }
                                        data["Item Name"] = item.menuitemname;
                                        data["Item count"] = item.count;
                                        data["Item Price"] = item.totalprice;
                                        let gokhanadiscountpercent = 0;
                                        let restaurantdiscountpercent = 0;
                                        let companydiscountpercent = 0;
                                        if (isNotEmpty(order.data.discountsplit)) {
                                            order.data.discountsplit.forEach(dis => {
                                                if (dis.percent || dis.percent == 0) dis.percent = order.data.paidamount + order.data.totalCompanyDiscount
                                                if (dis.promotedgroup == "gokhana" || dis.promotedby == "gokhana") {
                                                    gokhanadiscountpercent = dis.percent;
                                                } else if (dis.promotedgroup == "restaurant" || dis.promotedby == "restaurant") {
                                                    restaurantdiscountpercent = dis.percent;
                                                } else if (dis.promotedgroup == "company" || dis.promotedby == "company") {
                                                    companydiscountpercent = dis.percent;
                                                }
                                            });
                                        }
                                        restaurantdiscountpercent = calculateDisPercent(restaurantdiscountpercent, order.data.discountsplit, "restaurant", item.totalprice);
                                        restaurantdiscountamount = doubleDecimal((item.totalprice * restaurantdiscountpercent) * 0.01);
                                        let gstper = ((item.taxAmount / item.totalprice) * 100);
                                        totalgst = restaurantdiscountamount > 0 ? doubleDecimal(((item.totalprice - restaurantdiscountamount) * gstper) * 0.01) : item.taxAmount;
                                        netamount = (item.totalprice - restaurantdiscountamount) + totalgst;
                                        gokhanadiscountpercent = calculateDisPercent(gokhanadiscountpercent, order.data.discountsplit, "gokhana", netamount);
                                        gokhanadiscountamount = doubleDecimal((netamount * gokhanadiscountpercent) * 0.01);
                                        companydiscountpercent = calculateDisPercent(companydiscountpercent, order.data.discountsplit, "company", netamount - gokhanadiscountamount);
                                        if (companydiscountpercent > 0) companydiscountamount = doubleDecimal(((netamount - gokhanadiscountamount) * companydiscountpercent) * 0.01);
                                        let empid;
                                        let companyname;
                                        empid = order.data.employeeid;
                                        if (order.data.company_details && order.data.company_details.data) companyname = order.data.company_details.data.name;
                                        data["Restaurant Discount Amount"] = restaurantdiscountamount > 0 ? restaurantdiscountamount : "No discount";
                                        data["GST Amount"] = totalgst;
                                        data["Net Menu Item Amount"] = netamount;
                                        data["Gokhana Discount Amount"] = gokhanadiscountamount > 0 ? gokhanadiscountamount : "No discount";
                                        data["Employee Id"] = empid || "";
                                        data["Company Name"] = companyname || "";
                                        data["Company Discount Amount"] = companydiscountamount > 0 ? companydiscountamount : "No discount";
                                    } else {
                                        data = fillBlankData(data);
                                    }
                                    data["Payment Gateway"] = transaction.paymentgateway || '';
                                    data["Payment Type"] = transaction.paymenttype || 'Default';
                                    let paidamount = doubleDecimal(transaction.percent * (netamount - companydiscountamount - gokhanadiscountamount) * 0.01);
                                    let netAmountPerPaymentMode = order.data.paymenttransactions.length > 1 ? doubleDecimal(transaction.percent * netamount * 0.01) : netamount;
                                    let companydiscountamountPerPaymentMode = order.data.paymenttransactions.length > 1 ? doubleDecimal(transaction.percent * companydiscountamount * 0.01) : companydiscountamount;
                                    let gokhanadiscountamountPerPaymentMode = order.data.paymenttransactions.length > 1 ? doubleDecimal(transaction.percent * gokhanadiscountamount * 0.01) : gokhanadiscountamount;
                                    data["Amount paid by Customer"] = paidamount;
                                    var totalcommission = 0;
                                    if (cg && cg.data && isNotEmpty(cg.data.paymentmodes)) {
                                        let paymentmode;
                                        if (order.data.createdplatform == "gokhanacustomer") {
                                            paymentmode = cg.data.paymentmodes.find(o => o.apptype == "mobile")
                                        } else if (order.data.createdplatform == "gokhanapartnerfm-kiosk") {
                                            paymentmode = cg.data.paymentmodes.find(o => o.apptype == "kiosk")
                                        } else {
                                            paymentmode = cg.data.paymentmodes.find(o => o.name == transaction.paymenttype)
                                        }
                                        paymentmode.commissionsplit.forEach(commissions => {
                                            var tempcommission = 0;
                                            if (order.data.paymenttransactions.length > 0) {
                                                tempcommission = cg.data.includegst ? doubleDecimal((netAmountPerPaymentMode * commissions.value) * 0.01) : doubleDecimal(((item.totalprice / order.data.paymenttransactions.length - restaurantdiscountamount / order.data.paymenttransactions.length) * commissions.value) * 0.01)
                                            } else {
                                                tempcommission = cg.data.includegst ? doubleDecimal((netAmountPerPaymentMode * commissions.value) * 0.01) : doubleDecimal(((item.totalprice - restaurantdiscountamount) * commissions.value) * 0.01)
                                            }
                                            data[commissions.entityname + ' Commission %'] = commissions.value ? commissions.value : "NA";
                                            data[commissions.entityname + " Commission Amount"] = doubleDecimal(tempcommission);
                                            let commissionsgst = (tempcommission * (cg.data.tcs || (app_setting && app_setting.data && cg.data.tcs ? cg.data.tcs : 18))) * 0.01
                                            data["GST on " + commissions.entityname + " Commission Amount"] = doubleDecimal(commissionsgst);
                                            data["Net " + commissions.entityname + " Commission Amount"] = doubleDecimal(tempcommission + commissionsgst);
                                            totalcommission += doubleDecimal(tempcommission + commissionsgst);
                                        });
                                    }
                                    if (transaction.paymentgateway == "onsite" || transaction.paymentgateway == "vendor") {
                                        data['Amount to Merchant'] = doubleDecimal(-totalcommission + companydiscountamountPerPaymentMode + gokhanadiscountamountPerPaymentMode);
                                    } else {
                                        data['Amount to Merchant'] = doubleDecimal(netAmountPerPaymentMode - totalcommission);
                                    }
                                    isNotExistsPreviousRow = false;
                                    item.isComCalculate = true;
                                    if (data.OrderID.length > 0) {
                                        ids.push(data.OrderID);
                                    }
                                    ctr = ctr + 1;
                                    report.push(data);
                                }
                            })
                        }
                    }
                }
            }
        }
    }// console.log('coms inside: ', report.length);
    ctr = 1;
    return [report, issuelist, ids];
}

function calculateDisPercent(percent, discountsplit, type, amount) {
    if (percent == 0 && isNotEmpty(discountsplit)) {
        let dis = discountsplit.find(dis => dis.promotedgroup == type || dis.promotedby == type);
        if (dis) return calculatepercent(amount, dis.amounts);
    }
    return percent;
}

function fillBlankData(data) {
    data["OrderID"] = '';
    data["Token No"] = '';
    data["Placed Date"] = '';
    data["Placed Time"] = '';
    data["Restaurant"] = '';
    data["Foodcourt"] = '';
    data["Items Count"] = '';
    data["Order Value"] = '';
    data["Order Paid Amount"] = '';
    data["Status"] = '';
    data["Order ype"] = '';
    data["Payment Status"] = '';
    data["Created platform"] = '';
    data["Item Name"] = '';
    data["Item Price"] = '';
    data["Item count"] = '';
    data["Restaurant Discount Amount"] = '';
    data["GST Amount"] = '';
    data["Net Menu Item Amount"] = '';
    data["Gokhana Discount Amount"] = '';
    data["Employee Id"] = '';
    data["Company Name"] = '';
    data["Company Discount Amount"] = '';
    return data;
}

function createARowWithData(report, data) {
    if (isNotEmpty(report)) {
        let row = {}
        let _temp = _.cloneDeep(report);
        _temp.sort((a, b) => {
            if (Object.keys(a).length > Object.keys(b).length) return -1;
            else return 1;
        })
        let r = report.reduce((l, o) => {
            Object.keys(l).forEach(k => {
                if (!Object.keys(o).find(a => a == k)) {
                    o[k] = '';
                }
            })
            return o;
        })
        Object.keys(r).forEach(key => {
            row[key] = data;
        });
        return row;
    }
    return null;
}

function insertTotalAllCalculation(row, report, titleIsRestaurant) {
    let total = {};
    Object.keys(row).forEach(key => {
        if (key == "Total") total["Total"] = "Total";
        if (key == "Restaurant Name") total["Restaurant Name"] = "Total";
        if (needSkippedForTotalling(key)) {
            total[key] = doubleDecimal(report.reduce((aa, bb) => {
                if (bb && bb[key]) {
                    if (key == "OrderID") return aa + 1
                    return aa + toNumber(bb[key]);
                }
                return aa;
            }, 0)) || "";
        }
    });
    return total;
}

function needSkippedForTotalling(key) {
    return !isBlank(key) &&
        key != "Total" &&
        key != "parentId" &&
        key != "Token No" &&
        // key != "OrderID" &&
        key != "Token No" &&
        key != "Placed Date" &&
        key != "Placed Time" &&
        key != "Item Name" &&
        key != "External Transaction id" &&
        key != "Company Name" &&
        key != "Employee Id" &&
        key != "From Date" &&
        key != "Customer Ref No" &&
        key != "Beneficiary Code" &&
        key != "Beneficiary Name" &&
        key != "Beneficiary A/c No" &&
        key != "Beneficiary A/c Type" &&
        key != "Ifsc Code" &&
        key != "Bene Email ID" &&
        key != "Print Location" &&
        key != "Beneficiary add line1" &&
        key != "line1Beneficiary add line2" &&
        key != "To Date" &&
        key != "Restaurant Name" &&
        (key != "Commission %" || key.includes("Commission %")) &&
        key != "Payment Type";
}


async function getCommissionGrids(fromdate, todate, parentIds) {
    let should = [];
    if (isNotEmpty(parentIds)) {
        parentIds.forEach(parentId => {
            should.push({ "term": { "data.parentId.keyword": parentId } })
        })
    }
    // let COMISSION_GRID_QUERY = {
    //     "size": 10000,
    //     "from": 0,
    //     "query": {
    //         "bool": {
    //             "filter": [
    //                 {
    //                     "term": {
    //                         "appId": GOKHANA_APPID
    //                     }
    //                 },
    //                 {
    //                     "term": {
    //                         "entityId": RESTAURANT_COMMISSION_GRID_ENTITY_ID
    //                     }
    //                 }
    //             ],
    //             "should": [
    //                 {
    //                     "bool": {
    //                         "must": [
    //                             {
    //                                 "range": {
    //                                     "data.effectivestartdate": {
    //                                         "gte": fromdate
    //                                     }
    //                                 }
    //                             },
    //                             {
    //                                 "range": {
    //                                     "data.effectiveenddate": {
    //                                         "lte": todate
    //                                     }
    //                                 }
    //                             }
    //                         ]
    //                     }
    //                 },

    //                 {
    //                     "bool": {
    //                         "must": [
    //                             {
    //                                 "range": {
    //                                     "data.effectivestartdate": {
    //                                         "gte": fromdate
    //                                     }
    //                                 }
    //                             },
    //                             {
    //                                 "range": {
    //                                     "data.effectiveenddate": {
    //                                         "lte": fromdate
    //                                     }
    //                                 }
    //                             }
    //                         ]
    //                     }
    //                 },
    //                 {
    //                     "bool": {
    //                         "must": [
    //                             {
    //                                 "range": {
    //                                     "data.effectivestartdate": {
    //                                         "gte": todate
    //                                     }
    //                                 }
    //                             },
    //                             {
    //                                 "range": {
    //                                     "data.effectiveenddate": {
    //                                         "lte": todate
    //                                     }
    //                                 }
    //                             }
    //                         ]
    //                     }
    //                 }
    //             ]
    //         }
    //     }
    // };
    let COMISSION_GRID_QUERY = {
        "size": 10000,
        "from": 0,
        "query": {
            "bool": {
                "filter": [
                    {
                        "term": {
                            "appId": "5825d15cf009c4bc37fd7b9f"
                        }
                    },
                    {
                        "term": {
                            "entityId": "5bceffb7c6ff750411db480c"
                        }
                    }
                ],
                "should": []
            }
        }
    };
    return triggerElastic(COMISSION_GRID_QUERY);
}

function triggerElastic(query) {
    let tempData = [];
    return new Promise((resolve, reject) => {
        analyticsclient.search({ index: 'gokhanaprod', body: query, scroll: '30s' }, function getMore(error, response) {
            if (error) reject(error);
            tempData = [...tempData, ...(response.body.hits.hits.map(o => Object.assign({}, o._source)))];
            console.log("fetch data count ", tempData.length, "total data count", response.body.hits.total)
            if (response.body.hits.total !== tempData.length) {
                analyticsclient.scroll({ scroll_id: response.body._scroll_id, scroll: '30s' }, getMore);
            } else {
                resolve(tempData);
            }
        });
    });
}

async function getOrders(fromdate, todate, parentIds) {
    let should = [];
    // let must = [];
    // if (isNotEmpty(parentIds)) {
    // if (parentIds.length == 1) {
    // parentIds.forEach(parentId => {
    //     must.push({ "term": { "data.parentId.keyword": parentId } })
    // })
    // }
    // else {
    parentIds.forEach(parentId => {
        should.push({ "term": { "data.parentId.keyword": parentId } })
    })
    // }
    // }
    console.log('ord should: ', should);
    let ORDERS_QUERY =
    {
        "size": 10000,
        "from": 0,
        "query": {
            "bool": {
                "must": [],
                "filter": [
                    {
                        "term": {
                            "appId": GOKHANA_APPID
                        }
                    },
                    {
                        "term": {
                            "entityId": RESTAURANT_ORDER_ENTITY_ID
                        }
                    },
                    {
                        "range": {
                            "data.placedtime": {
                                "gte": fromdate,
                                "lte": todate
                            }
                        }
                    }
                ],
                "should": should,
                "must_not": []
            }
        },
        "sort": [
            {
                "data.placedtime": "desc"
            }
        ]
    };
    // let ORDERS_QUERY = {
    //     "size": 10000,
    //     "from": 0,
    //     "query": {
    //         "bool": {
    //             "must": [],
    //             "filter": [
    //                 {
    //                     "term": {
    //                         "appId": GOKHANA_APPID
    //                     }
    //                 },
    //                 {
    //                     "term": {
    //                         "entityId": RESTAURANT_ORDER_ENTITY_ID
    //                     }
    //                 },
    //                 {
    //                     "range": {
    //                         "data.placedtime": {
    //                             "gte": fromdate,
    //                             "lte": todate
    //                         }
    //                     }
    //                 }
    //             ],
    //             "should": isNotEmpty(should) ? [should] : []
    //         }
    //     },
    //     "sort": [
    //         {
    //             "data.placedtime": "desc"
    //         }
    //     ]
    // };
    console.log(JSON.stringify(ORDERS_QUERY));
    return triggerElastic(ORDERS_QUERY)
}

function isNotEmpty(array) {
    if (array && Array.isArray(array) && array.length > 0) {
        return true;
    }
    return false;
}

function isEmpty(array) {
    if (!array || (Array.isArray(array) && array.length == 0)) {
        return true;
    }
    return false;
}

function getArrayLength(array) {
    if ((Array.isArray(array))) {
        return array.length;
    }
    return 0;
}

function isNotBlank(value) {
    return (value && value != '');
}

function isBlank(value) {
    return (!value || value == '');
}

function toNumber(value) {
    let result = Number(value);
    if (isNaN(result)) {
        return 0;
    }
    return result;
}

function doubleDecimal(value) {
    return toNumber(value.toFixed(2));
}

function calculatepercent(top, base) {
    if (toNumber(top) && toNumber(base)) {
        return doubleDecimal(toNumber(top / base * 100));
    } else {
        return null;
    }
}


async function calculateAllOrdersCommission(commissions, fromdate, todate, parentIds, emails) {
    let filteredOrders = 0;
    let reports = [];
    let issuelists = [];
    return getOrders(fromdate, todate, parentIds).then(orders => {
        if (isNotEmpty(orders)) {
            if (isNotEmpty(commissions)) {
                let check = [];
                commissions.forEach(com => {
                    let _orders = orders.filter(o => o.data.parentId == com.data.parentId);
                    // console.log('com to be cal: ', _orders.length);
                    if (isNotEmpty(_orders)) {
                        let [report, issuelist, ids] = calculateCommission(com, _orders, fromdate, todate);
                        if (isNotEmpty(report)) reports = [...reports, ...report];
                        if (isNotEmpty(issuelist)) issuelists = [...issuelists, ...issuelist]
                        // check.push(_orders[0].data.parentId);
                        if (isNotEmpty(ids)) check = [...check, ...ids];
                        // console.log('coms len: ', reports.length);
                        filteredOrders = filteredOrders + _orders.length;
                    }
                });
                console.log('issuelists: ', issuelists);
                console.log('orders shortlisted: ', filteredOrders);
                console.log('all coms: ', reports.length);
                // console.log(check);
                if (isNotEmpty(check)) {
                    // console.log('check len: ', check.length);
                    // console.log('orders-len: ', orders.length);
                    // console.log();
                    // console.log(check.find(parentId => { return parentId != orders[0].data.parentId }));
                    // let _orders = orders.filter(order => { return check.find(parentId => { return parentId != order.data.parentId }) });
                    let _orders = [];
                    let ctr = 0
                    for (let l = 0; l < orders.length; l++) {
                        if (check.indexOf(orders[l].data.orderid) == -1) {
                            _orders.push(orders[l]);
                        }
                        else ctr = ctr + 1;
                    }
                    console.log('removed: ', ctr);
                    console.log('_orders-len: ', _orders.length);

                    if (isNotEmpty(_orders)) {
                        let _report = calculateWithOutCommission(_orders);
                        console.log("_report", _report.length);
                        console.log("reports", reports.length);
                        if (isNotEmpty(_report)) reports = [...reports, ..._report];
                    }
                }
                if (isNotEmpty(reports)) {
                    console.log("reports 1", reports.length)
                    return _calculateAllOrdersCommission(reports, issuelists, fromdate, todate, emails);
                } else {
                    let _report = calculateWithOutCommission(orders);
                    if (isNotEmpty(_report)) reports = [...reports, ..._report]
                    if (isNotEmpty(reports)) {
                        console.log("reports 2", reports.length)
                        return _calculateAllOrdersCommission(reports, issuelists, fromdate, todate, emails)
                    } else {
                        return Promise.reject("No Reports");
                    }
                }
            } else {
                let _report = calculateWithOutCommission(orders);
                if (isNotEmpty(_report)) reports = [...reports, ..._report]
                if (isNotEmpty(reports)) {
                    console.log("reports 3", reports.length)
                    return _calculateAllOrdersCommission(reports, issuelists, fromdate, todate, emails)
                } else {
                    return Promise.reject("No Orders");
                }
            }
        } else {
            return Promise.reject("No Orders");
        }
    })
    return Promise.reject("No commissions");
}

function _calculateAllOrdersCommission(reports, issuelists, fromdate, todate, emails) {
    console.log("start _calculateAllOrdersCommission");
    return createAndUploadFinalRecords([reports, issuelists], new Date(), fromdate, todate).then(o => {
        return sendEmail(o.Location, fromdate, todate, emails);
    })
}

// function destroyObjects() {

// }

function filterComissions(fromdate, todate, commissions) {
    return new Promise(function (resolve, reject) {
        if (commissions) {
            let newcoms = [];
            commissions.forEach(hit => {
                // console.log(hit.data);
                if ((moment(hit.data.effectiveenddate).isAfter(fromdate) && moment(hit.data.effectiveenddate).isBefore(todate)) ||
                    (moment(hit.data.effectivestartdate).isAfter(fromdate) && moment(hit.data.effectivestartdate).isBefore(todate)) ||
                    (moment(hit.data.effectivestartdate).isBefore(fromdate) && moment(hit.data.effectiveenddate).isAfter(todate))) {
                    newcoms.push(hit);
                }
            });
            console.log(newcoms.length);
            resolve(newcoms);
        }
        else reject("No comissions");
    });
}

async function PrepareCGJSON(rawCG) {
    return new Promise(function (resolve, reject) {
        let partyCounter = {}
        let finalCGJSON = []
        //find stakeholders
        rawCG[0]['data'].forEach((party) => {
            if (party != null) {
                if (party in partyCounter)
                    partyCounter[party]++
                else
                    partyCounter[party] = 0
            }
        })

        // console.log(rawCG[2]["data"])

        if (Object.keys(partyCounter).length > 0) {
            for (let x = 2; x < rawCG.length; x++) {
                if (rawCG[x]['data'][1]) {
                    let incr = 8
                    let tempArr = {
                        "data": {
                            "effectivestartdate": moment(rawCG[x]['data'][4]).startOf('D'),
                            "effectiveenddate": moment(rawCG[x]['data'][5]).endOf('D'),
                            "includegst": rawCG[x]['data'][4] != 0 ? true : false,
                            "tcs": parseInt(rawCG[x]['data'][6] * 100),
                            "parentId": rawCG[x]['data'][1],
                            "paymentmodes": [{
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][7] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][15] * 100)
                                    }
                                ],
                                "name": "cash",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][8] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][16] * 100)
                                    }
                                ],
                                "name": "paytm-cash",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][8] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][16] * 100)
                                    }
                                ],
                                "name": "paytm-postpaid-wallet",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][9] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][17] * 100)
                                    }
                                ],
                                "name": "credit-card",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][9] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][17] * 100)
                                    }
                                ],
                                "name": "debit-card",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][10] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][18] * 100)
                                    }
                                ],
                                "name": "niyo",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][11] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][19] * 100)
                                    }
                                ],
                                "name": "gpay",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][12] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][20] * 100)
                                    }
                                ],
                                "name": "sodexo",
                                "apptype": ""
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][12] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][20] * 100)
                                    }
                                ],
                                "name": "mobile",
                                "apptype": "mobile"
                            },
                            {
                                "commissionsplit": [
                                    {
                                        "entityname": 'Gokhana',
                                        "value": parseInt(rawCG[x]['data'][12] * 100)
                                    },
                                    {
                                        "entityname": 'Vendor',
                                        "value": parseInt(rawCG[x]['data'][20] * 100)
                                    }
                                ],
                                "name": "kiosk",
                                "apptype": "kiosk"
                            }]
                        }
                    }
                    // Object.keys(partyCounter).forEach((sh) => {
                    // tempArr.data.groupings[0].paymentmodes = [
                    // ]
                    // })
                    finalCGJSON.push(tempArr)
                }
            }
            console.log('CG generated from CG file !')
            resolve(finalCGJSON)
            // console.log(JSON.stringify(finalCGJSON))

        } else reject('Unexpected excel formatting !');
    })
}


async function ReadCommissionGridFile(fileWithPath) {
    let comGrid = [];
    var wb = new Excel.Workbook()
    return new Promise(function (resolve, reject) {
        wb.xlsx.readFile(fileWithPath)
            .then(function () {
                var worksheet = wb.getWorksheet('Sheet1', {})
                worksheet.eachRow({ includeEmpty: true }, function (row, rowNumber) {
                    comGrid.push({ "data": row.values })
                })
                resolve(comGrid)
            })
            .catch((err) => { console.log('The error: ', err) })
    });
}

// exports.handler = () => {
(function () {
    return request('https://api.ampcome.com/api/gokhana/appsetting', {
        method: 'get',
        headers: {
            'Authorization': 'nJlpU4e9jOMKJYegSxzzYdIQ6zlsTCbYFDfykNmK7x6Wqq7WKlDV1Jp4x9YiERP9'
        }
    }, function (err, res, body) {
        if (!err) {
            if (JSON.parse(body).settlement_reports_summary_sento) {
                // let emails_list = JSON.parse(body).settlement_reports_summary_sento;
                let emails_list = "prashant@ampcome.com";
                let parentIds = [];
                let fromdate = "2014-05-19T18:30:00.000Z";
                let todate = "2020-05-23T18:29:59.000Z";
                let emails = [];

                emails_list.split(',').forEach((email) => {
                    emails.push(email.trim());
                });

                // let parentIds = [];
                // if (isNotEmpty(emails)) { }
                // return getCommissionGrids(fromdate, todate, parentIds).then(commissions => {
                // console.log(JSON.stringify(commissions))
                return ReadCommissionGridFile('cg.xlsx').then((rawCG) => {
                    return PrepareCGJSON(rawCG).then((commissions) => {
                        return filterComissions(fromdate, todate, commissions).then((newcommissions) => {
                            // console.log('coms len: ', commissions.length);
                            // console.log('coms len: ', JSON.stringify(newcommissions));
                            return calculateAllOrdersCommission(newcommissions, fromdate, todate, parentIds, emails)
                                .then(res => {
                                    return console.log(res);
                                    // destroyObjects();
                                    // return sendRes(200, res.toString());
                                }).catch(err => {
                                    return console.log("Error", err);
                                    // destroyObjects();
                                    // return sendRes(500, err.toString());
                                })
                        }).catch(err => { return console.log("Error", err); });
                    }).catch((e) => { console.log(e) })
                }).catch(err => {
                    return console.log("Error", err);
                    // destroyObjects();
                    // return sendRes(500, err.toString());
                });
            }
        }
    });
})();
// }
// exports.handler = async (event) => {
//     request('https://api.ampcome.com/api/gokhana/appsetting', {
//         method: 'get',
//         headers: {
//             'Authorization': 'nJlpU4e9jOMKJYegSxzzYdIQ6zlsTCbYFDfykNmK7x6Wqq7WKlDV1Jp4x9YiERP9'
//         }
//     }, function (err, res, body) {
//         if (!err) {
//             if (JSON.parse(body).settlement_reports_summary_sento) {
//                 let emails_list = JSON.parse(body).settlement_reports_summary_sento;
//                 let parentIds = [];
//                 let fromdate = '2015-05-08T18:30:00.000Z';
//                 let todate = '2030-05-09T18:29:59.000Z';
//                 let emails = [];

//                 emails_list.split(',').forEach((email) => {
//                     emails.push(email.trim());
//                 });

//                 // let parentIds = [];
//                 // if (isNotEmpty(emails)) { }
//                 getCommissionGrids(fromdate, todate, parentIds).then(commissions => {
//                     return filterComissions(fromdate, todate, commissions).then((newcomissions) => {
//                         // console.log('coms len: ', commissions.length);
//                         // console.log('coms len: ', newcomissions.length);
//                         calculateAllOrdersCommission(newcomissions, fromdate, todate, parentIds, emails)
//                             .then(res => {
//                                 console.log(res);
//                                 // destroyObjects();
//                                 // return sendRes(200, res.toString());
//                             }).catch(err => {
//                                 console.log("Error", err);
//                                 // destroyObjects();
//                                 // return sendRes(500, err.toString());
//                             })
//                     });
//                 }).catch(err => {
//                     console.log("Error", err);
//                     // destroyObjects();
//                     // return sendRes(500, err.toString());
//                 });
//             }
//         }
//     });
// }
// })();